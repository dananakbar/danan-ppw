from django.test import TestCase
from django.urls import resolve
from .views import index

# Create your tests here.

class LoginPageTesting(TestCase):
    def test_login_page_exists(self):
        response = Client().get('/login_page/')
        self.assertEquals(response.status_code, 200)
    
    def test_login_page_uses_templates(self):
        response = Client().get('/login_page/')
        self.assertTemplateUsed(response, 'login_page.html')

    def test_login_page_uses_index_func(self):
        found = resolve('/login_page/')
        self.assertEquals(found.func, index)
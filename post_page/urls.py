from django.urls import path

from . import views

app_name = 'post_page'

urlpatterns = [
    path('', views.post_page, name='post_page')
]
from django.shortcuts import render

# Create your views here.

def index(request):
    context = {
        'page' : 'informations'
    }
    return render(request, 'info-index.html', context)
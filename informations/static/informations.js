function openPanel(accId, panelId){

    // accordion.classList.toggle("active");
    var accordion = document.getElementById(accId);
    var panel = document.getElementById(panelId);

    if (panel.style.display === "block") {
        panel.style.display = "none";
        accordion.style.borderRadius = "5px";            
    } 
    else {
        var accList = document.getElementsByClassName("accordion");
        var i;
        var targetPanel;
        for (i = 0; i < accList.length; i++){
            targetPanel = accList[i].nextElementSibling;
            targetPanel.style.display = "none";
            accList[i].style.borderRadius = "5px";    
        }
        panel.style.display = "block";
        accordion.style.borderBottomLeftRadius = "0";
        accordion.style.borderBottomRightRadius = "0";
    }
}

function accUp(accContId){
    var mainAccContainer = document.getElementById(accContId);
    var accSection = mainAccContainer.parentElement;
    var prevAccContainer = mainAccContainer.previousElementSibling;

    if (prevAccContainer != null){
        accSection.insertBefore(mainAccContainer, prevAccContainer);
    }
}

function accDown(accContId){
    var mainAccContainer = document.getElementById(accContId);
    var accSection = mainAccContainer.parentElement;
    var nextAccContainer = mainAccContainer.nextElementSibling;

    if (nextAccContainer != null){
        accSection.insertBefore(nextAccContainer, mainAccContainer);
    }
}
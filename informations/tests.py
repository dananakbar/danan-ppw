from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.

class InformationsTesting(TestCase):
    def test_informations_exists(self):
        response = Client().get('/informations/')
        self.assertEquals(response.status_code, 200)
    
    def test_informations_uses_templates(self):
        response = Client().get('/informations/')
        self.assertTemplateUsed(response, 'info-index.html')

    def test_informations_uses_index_func(self):
        found = resolve('/informations/')
        self.assertEquals(found.func, index)
    
    # def 
    




# Generated by Django 3.1.1 on 2020-10-17 01:24

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=60, verbose_name='Nama Mata Kuliah')),
                ('dosen', models.CharField(max_length=60, verbose_name='Dosen Pengajar Matkul')),
                ('jumlah_sks', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(10)], verbose_name='SKS Mata Kuliah')),
                ('deskripsi', models.TextField(blank=True)),
                ('smt_diambil', models.CharField(max_length=20, verbose_name='Semester Matkul Diambil')),
                ('r_kelas', models.CharField(max_length=20, verbose_name='Ruang Kelas Matkul')),
            ],
        ),
    ]

from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator
from .models import Matkul

class MatkulForm(forms.ModelForm) :
        class Meta :
                model = Matkul
                fields = ['nama', 'dosen', 'jumlah_sks', 'deskripsi',
                'smt_diambil', 'r_kelas']
        nama = forms.CharField(label='Nama Mata Kuliah', max_length=60) 
        dosen = forms.CharField(label='Dosen Pengajar Matkul', max_length=60) 
        jumlah_sks = forms.IntegerField(label='SKS Mata Kuliah', min_value=1, max_value=10 ) 
                # validators=[MinValueValidator(1), MaxValueValidator(10)]
        deskripsi = forms.CharField(label='Deskripsi Mata Kuliah') 
        smt_diambil = forms.CharField(label='Semester Matkul Diambil', max_length=20) 
        r_kelas = forms.CharField(label='Ruang Kelas Matkul', max_length=20)


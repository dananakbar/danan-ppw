from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import MatkulForm

# Create your views here.

def savematkul(request):
    # if this is a POST request we need to process the form data
    form = MatkulForm(request.POST or None)

    if (form.is_valid and request.method == 'POST'):
        form.save()
        return redirect('courses:courses')
    else :
        return render(request, 'courses.html', {'page' : 'courses', 'matkul_form' : MatkulForm})

    #     # create a form instance and populate it with data from the request:
    #     form = MatkulForm(request.GET)
    #     # check whether it's valid:
    #     if form.is_valid():
    #         form.save()
    #         return HttpResponseRedirect('courses/')

    # # if a GET (or any other method) we'll create a blank form
    # else:
    #     form = NameForm()

    # return render(request, 'name.html', {'form': form})
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class Matkul(models.Model):
	nama = models.CharField('Nama Mata Kuliah', max_length=60)
	dosen = models.CharField('Dosen Pengajar Matkul', max_length=60)
	jumlah_sks = models.PositiveIntegerField('SKS Mata Kuliah', validators=[MinValueValidator(1), MaxValueValidator(10)])
	deskripsi = models.TextField(blank=True)
	smt_diambil = models.CharField('Semester Matkul Diambil', max_length=20)
	r_kelas = models.CharField('Ruang Kelas Matkul', max_length=20)
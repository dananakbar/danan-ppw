from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def index(request):
    context = {'page':'book_search'}
    return render(request, "booksearch.html", context)

def book_result(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET["q"]
    data = requests.get(url)
    content = json.loads(data.content)
    return JsonResponse(content, safe=False)
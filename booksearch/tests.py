from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.

class BookSearchTesting(TestCase):
    def test_booksearch_exists(self):
        response = Client().get('/booksearch/')
        self.assertEquals(response.status_code, 200)
    
    def test_booksearch_uses_templates(self):
        response = Client().get('/booksearch/')
        self.assertTemplateUsed(response, 'booksearch.html')

    def test_booksearch_uses_index_func(self):
        found = resolve('/booksearch/')
        self.assertEquals(found.func, index)
    
    def test_booksearch_shows_requested_books(self):
        context = {'book_title':'scott pilgrim'}
        response = Client().get('/booksearch/', data=context)
        self.assertEquals(response.status_code, 200)
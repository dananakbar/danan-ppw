$( document ).ready(
    $("#searchBox").keyup( 
        function(){
            var searchValue = $("#searchBox").val();
            
            $.ajax({ 
                url: "/booksearch/book-result?q=" + searchValue,
                success: function(data){
                    console.log(data);
                    var i = 0;
                    $("#bookList").empty();
                    for (i; i < 10; i++){
                        var bookLink = data.items[i].volumeInfo.canonicalVolumeLink;
                        var title = data.items[i].volumeInfo.title;
                        var img_link = data.items[i].volumeInfo.imageLinks.thumbnail;
                        var authors = data.items[i].volumeInfo.authors;
                        var publisher = data.items[i].volumeInfo.publisher;
                        var year = data.items[i].volumeInfo.publishedDate;
                        var description = data.items[i].volumeInfo.description;
                        // var bookLink = data.items[i].canonicalVolumeLink;

                        var mainString = '<div class="bookView-container">'

                        mainString += `<div class="img-button-container">
                            <div class="bookImg-container">
                                <img src=${img_link} class="bookImg">
                            </div>
                            <div class="visit-button-container">
                                <a href="${bookLink}">
                                <button type="button" class="book-button">Dapatkan</button>
                                </a>
                            </div>
                        </div>`


                        mainString += '<div class="bookDesc">';

                        if (title != null){
                            mainString += `<h3 class="bookTitle">${title}</h3>`;
                        }
                        
                        mainString += '<p>';

                        if (authors != null){
                            var str = `Ditulis oleh <span class="bookDetails">${authors[0]}`;
                            var j = 1;
                            for (j; j < authors.length - 1; j++){
                                str += `, ${authors[j]}`
                            }

                            if (authors.length == 2) str += ` dan ${authors[j]}`

                            if (authors.length > 2) str += `, dan ${authors[authors.length - 1]}`

                            str += `</span><br>`;
                            mainString += str;
                        }
                        
                        if (publisher != null){
                            mainString += `Diterbitkan oleh <span class="bookDetails">${publisher}</span><br>`;
                        }
                        
                        if (year != null){
                            mainString += `Diterbitkan pada <span class="bookDetails">${year}</span><br>`;
                        }
                        if (description != null){
                            mainString += `<br>${description}`;
                        }
                        
                        

                        mainString += '</p>';               
                        
                        mainString += '</div></div>';
                        
                        // console.log(bookLink);


                        $("#bookList").append(mainString);
                    }
                }
            });
        }
    )
);